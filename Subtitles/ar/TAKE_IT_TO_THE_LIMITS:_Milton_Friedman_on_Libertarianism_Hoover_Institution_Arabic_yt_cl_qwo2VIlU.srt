1
00:00:41,520 --> 00:00:44,130
مرحباً بكم في برنامج المعرفة غير الشائعة، أنا بيتر روبنسون

2
00:00:44,460 --> 00:00:46,713
حلقتنا اليوم تدور حول الليبرتارية

3
00:00:46,714 --> 00:00:50,707
ضيفنا هو الحائز على جائزة نوبل، الخبير الاقتصادي ميلتون فريدمان

4
00:00:50,707 --> 00:00:53,820
بالنسبة لملايين المواطنين الأمريكيين، يمثل ركوب الدراجة: معنى الحرية

5
00:00:53,821 --> 00:00:55,128
الفردية المطلقة

6
00:00:55,129 --> 00:00:57,659
متعة الزئير على الطرقات المفتوحة

7
00:00:57,660 --> 00:01:01,550
والهواء يتدفق من خلال شعرك

8
00:01:02,100 --> 00:01:03,390
حسناً، سيتدفق من خلال شعرك

9
00:01:03,390 --> 00:01:04,620
إذا لم ترتدي الخوذة

10
00:01:05,400 --> 00:01:08,160
كل راكبي الدرجات لديهم الخيار

11
00:01:08,610 --> 00:01:11,070
إما ارتداء الخوذة والحصول على أكبر قدر من الأمان

12
00:01:11,070 --> 00:01:13,080
أو نزع الخوذة والحصول على متعة أكثر

13
00:01:13,680 --> 00:01:15,420
جميع راكبي الدرجات لديهم حرية الاختيار

14
00:01:15,420 --> 00:01:17,320
ما عدا في أكثر من عشرين ولاية

15
00:01:17,320 --> 00:01:19,570
ومن ضمنهم ولاية كاليفورنيا

16
00:01:19,571 --> 00:01:21,600
التي تفرض ارتداء الخوذة بالقانون

17
00:01:21,640 --> 00:01:23,470
وهذا هو موضوع

18
00:01:23,460 --> 00:01:25,750
المشكلة التي سنتحدث عنها مع ميلتون فريدمان اليوم

19
00:01:25,750 --> 00:01:28,688
كونه ليبريتاري دكتور فريدمان يؤمن

20
00:01:28,689 --> 00:01:31,679
بأقصى حرية ممكنة للفرد

21
00:01:31,680 --> 00:01:35,342
ولكنه أيضا يدرك الحاجة إلى مهام حكومية معينة

22
00:01:35,528 --> 00:01:37,070
أية مهام بالتحديد؟

23
00:01:37,340 --> 00:01:41,990
كيف يقرر متى يشرع للحكومة أن تسلب بعض حرياتنا

24
00:01:42,540 --> 00:01:45,640
لن نتحدث أنا ودكتور فريدمان عن خوذة الدراجة

25
00:01:45,774 --> 00:01:47,999
لكننا سنطرح قضايا أكبر

26
00:01:48,000 --> 00:01:50,579
عن نظرة الليبرتارية للصحة العامة

27
00:01:50,580 --> 00:01:51,844
حماية البيئة

28
00:01:51,845 --> 00:01:54,814
أو تحديد الحجم الصحيح للحكومة في حد ذاتها

29
00:01:54,815 --> 00:01:59,830
نبدأ بسؤال دكتور فريدمان عن ماهية الليبرتارية

30
00:02:04,020 --> 00:02:08,093
التعريف النموذجي لليبرتارية من وجهة نظري

31
00:02:08,094 --> 00:02:12,064
هو أن الليبرتارية ترغب بأصغر حكومة وأقلها تدخلاً

32
00:02:12,065 --> 00:02:20,438
،تزامناً مع أقصى حرية للفرد ليتابع طريقه الخاص

33
00:02:20,439 --> 00:02:21,473
.قيمه الشخصية

34
00:02:21,474 --> 00:02:25,190
ما دام لم يمنع أحداً أخر من فعل ذلك أيضا

35
00:02:25,300 --> 00:02:26,710
إذا بالملائمة مع

36
00:02:26,700 --> 00:02:28,738
أقصى حرية للفرد

37
00:02:28,738 --> 00:02:32,848
ما لم تتداخل مع الأفراد الأخرين الساعين لأجل حريتهم الشخصية.

38
00:02:32,849 --> 00:02:37,180
لكن في الواقع هناك نسختان مختلفتان من الليبرتارية

39
00:02:37,181 --> 00:02:41,479
النسخة الأكثر تطرفاً تمتلك مبدأ أساسي

40
00:02:41,480 --> 00:02:46,560
وهي أنه من غير الأخلاقي الفرض بالقوة على أحد أخر

41
00:02:47,130 --> 00:02:49,680
وهذا هو المنظور الرئيسي

42
00:02:49,950 --> 00:02:52,296
المتواجد في نوع آين راند من الليبرتارية

43
00:02:52,296 --> 00:02:54,887
إذا السلطة القسرية للولاية، هي غير أخلاقية في حد ذاتها

44
00:02:54,888 --> 00:02:59,609
وكل ما تحتاج لمعرفته لإدراك ما إذا كان ما تفعله الولاية غير أخلاقي،

45
00:02:59,610 --> 00:03:02,146
هو وجود اجبار بالقوة أم لا.

46
00:03:02,146 --> 00:03:03,510
هذا أحد الأنواع

47
00:03:03,990 --> 00:03:08,530
هناك نوع أخر أفضله

48
00:03:08,660 --> 00:03:12,024
والذي تطلق عليه الليبرتارية المئالية

49
00:03:12,025 --> 00:03:14,393
وهي ما قد قمت بتعريفه الآن

50
00:03:14,394 --> 00:03:17,359
حسنا أنت قمت بتعريفه حالا ولكن شكرا لك سوف أحصل على الفضل لي

51
00:03:17,370 --> 00:03:19,820
أرى طريقة عملك مع الطلبة الخريجين

52
00:03:20,280 --> 00:03:21,868
الآن إذا سمحت لي،

53
00:03:21,869 --> 00:03:25,170
دعني آخذك إلى سلسلة من الأسئلة

54
00:03:25,171 --> 00:03:27,300
والتي تطوف في خاطر العقل المعاصر

55
00:03:27,690 --> 00:03:30,120
وأسألك عن نظرة الليبريتالي تجاهها

56
00:03:30,990 --> 00:03:32,378
السؤال الأول

57
00:03:32,379 --> 00:03:34,297
البيئة

58
00:03:34,298 --> 00:03:40,000
الان قد يطرأ لكثير من ساكني مانهاتن  أن سنترال بارك مكان مهم لحياتهم

59
00:03:40,440 --> 00:03:42,399
ولو تُرك الأمر لميلتون فريدمان

60
00:03:42,400 --> 00:03:44,323
ستنقل ملكيتها للسوق

61
00:03:44,324 --> 00:03:47,890
وتُدفن تحت ناطحات السحاب ومواقف السيارات

62
00:03:47,890 --> 00:03:51,849
في غضون ثمانية عشر شهراً، أو أيا كانت المدة التي سيقوم فيها دونالد ترامب بتشييد هذه المباني

63
00:03:52,269 --> 00:03:58,030
المحافظة على المسارح في نيويورك لا يتطلب تدخل وكالة حكومية

64
00:03:58,590 --> 00:04:04,349
ليس هنالك حاجة إلى  تدخل وكالة حكومية للمحافظة على متاحف الفنون في نيويورك

65
00:04:04,350 --> 00:04:08,401
متحف الفن المعاصر ليس متحف حكومي، هو ملكية خاصة

66
00:04:08,401 --> 00:04:10,316
يوجد نوعان

67
00:04:10,317 --> 00:04:14,329
هناك مؤسسات ربحية ومؤسسات غير ربحية

68
00:04:14,330 --> 00:04:16,739
مثل المتحف والأوبرا وغيرها

69
00:04:16,740 --> 00:04:17,156
صحيح

70
00:04:17,157 --> 00:04:19,009
بنفس الطريقة

71
00:04:19,010 --> 00:04:21,489
لو أن الحدائق المركزية لم تمتلكها الحكومة

72
00:04:21,490 --> 00:04:24,628
لم تكن لتصبح المكان القذر الذي صارت إليه

73
00:04:24,629 --> 00:04:28,349
لقد نسيت ما حدث إلى سنترال بارك

74
00:04:28,349 --> 00:04:31,195
لمدة سنوات

75
00:04:31,195 --> 00:04:32,888
مضت منذ فترة، أقمنا

76
00:04:33,840 --> 00:04:36,419
على سنترال بارك ويست عندما كنا في نيويورك

77
00:04:36,419 --> 00:04:37,320
محل إقامة جميل

78
00:04:37,321 --> 00:04:38,010
خلال الحرب

79
00:04:38,011 --> 00:04:39,739
حتى بوقتها كان محل إقامة جيد جدا

80
00:04:39,739 --> 00:04:41,700
لم يكن سيء، ولكن لم يكن جيد على وجه الخصوص

81
00:04:41,760 --> 00:04:41,981
حسناً

82
00:04:41,982 --> 00:04:44,490
ولكننا كنا قادرين على أخذ أبناءنا إلى الحديقة

83
00:04:44,790 --> 00:04:47,280
عندما كانوا صغار وكنا نتركهم

84
00:04:47,400 --> 00:04:49,389
مع مربية شابة

85
00:04:49,389 --> 00:04:52,000
ولم يكن أحد قلقا

86
00:04:52,001 --> 00:04:55,200
ولكن مؤخراً

87
00:04:55,201 --> 00:04:58,999
سنترال بارك أصبحت المكان الذي لن تجرؤ فيه على فعل ذلك

88
00:04:59,000 --> 00:05:00,269
لم يكن أمناً

89
00:05:00,270 --> 00:05:02,519
وهذا بسبب كونها حديقة حكومية

90
00:05:02,520 --> 00:05:04,737
هذا هو المبدأ الأساسي

91
00:05:04,738 --> 00:05:10,230
لا أحد يهتم بملكية الغير كما يهتم بملكيته الشخصية

92
00:05:10,503 --> 00:05:13,830
لو أن سنترال بارك ملكية خاصة

93
00:05:14,120 --> 00:05:18,207
سيكون من المفضّل ايجاد مساحات ترويحية

94
00:05:18,218 --> 00:05:20,219
الان قد لامست شيء مهم للغاية

95
00:05:20,220 --> 00:05:22,531
لأن أحد الأشياء التي أحاول تمييزها هنا

96
00:05:22,532 --> 00:05:28,547
هي إلى أي مدى تعتبر الليبريتالية موقفاً أخلاقياً

97
00:05:28,547 --> 00:05:31,804
أنت تقوم بفعل ذلك لأنه حق وعادل

98
00:05:31,805 --> 00:05:33,750
ويخلق ذلك أفضل شروط العدل

99
00:05:34,290 --> 00:05:35,834
وتمضي بعمله

100
00:05:35,845 --> 00:05:37,504
لأنه يعمل

101
00:05:37,505 --> 00:05:40,973
ويبدو لي أنك قد غطيت الجانبين

102
00:05:41,000 --> 00:05:41,808
بالتأكيد

103
00:05:41,809 --> 00:05:44,910
الأمر الأساسي أنه إذا لم تعمل

104
00:05:46,530 --> 00:05:48,414
سيصبح ذلك هدف مستحيل

105
00:05:48,425 --> 00:05:50,917
والسبب الوحيد الكافي

106
00:05:50,918 --> 00:05:55,921
لابقاء الحكومة محدودة، هو أنها في  غاية اللافعالية

107
00:05:55,922 --> 00:05:57,409
وأدائها سيئ

108
00:05:57,410 --> 00:05:58,559
والآن دعني أجرب.

109
00:05:58,560 --> 00:06:00,594
في خلال الثورة الصناعية في القرن التاسع عشر

110
00:06:00,595 --> 00:06:02,930
الحكومة البريطانية كانت محدودة للغاية

111
00:06:02,931 --> 00:06:05,580
والمشاريع الاقتصادية كانت غير منظمة

112
00:06:06,240 --> 00:06:09,130
لكن ذلك لم يكن عصراً ذهبياً، أليس كذلك؟

113
00:06:12,060 --> 00:06:13,620
ومجددا ستسمع

114
00:06:13,620 --> 00:06:14,273
لقد جربنا

115
00:06:14,274 --> 00:06:16,075
العالم الغربي قد جرب بالفعل

116
00:06:16,076 --> 00:06:19,380
اقتصاديات لاسيز فير دعها تنطلق (بعدم التدخل)

117
00:06:19,620 --> 00:06:21,870
والتي انتهت بلندن

118
00:06:22,140 --> 00:06:27,720
التي صورها تشارليز ديكنز بوجود القذارة، وعمالة الأطفال

119
00:06:27,721 --> 00:06:29,289
مجرد فوضى عارمة

120
00:06:29,290 --> 00:06:30,209
ماذا ستفعل حينها

121
00:06:30,510 --> 00:06:32,280
حسنا ما الذي جعل ذلك يحدث؟

122
00:06:32,370 --> 00:06:34,393
لقد كانت فوضى فظيعة لكن ما الذي قام بتنظيفها

123
00:06:34,394 --> 00:06:36,180
ديزرالي

124
00:06:36,180 --> 00:06:38,430
و قوانين عمل الأطفال الاجتماعية

125
00:06:38,430 --> 00:06:42,268
لا، إن ما قام بتنظيفها هو تطور المشاريع الخاصة

126
00:06:42,269 --> 00:06:43,943
لأن هنالك

127
00:06:43,944 --> 00:06:46,110
سبب الفوضى

128
00:06:46,110 --> 00:06:47,970
هو حاجتك لحرق الفحم

129
00:06:48,570 --> 00:06:50,810
ونوعية الفحم الذي كان متواجداً في بريطانيا

130
00:06:50,811 --> 00:06:52,879
كان شديد الدخان وفوضوياً

131
00:06:52,880 --> 00:06:55,020
وبمجرد تمكنك من استخدام

132
00:06:56,160 --> 00:06:59,386
البترول، الغاز الطبيعي، وأفران أفضل

133
00:06:59,387 --> 00:07:04,357
كل هذه الأشياء هي ما جعلت تنظيف لندن ممكناً

134
00:07:04,358 --> 00:07:06,759
وبالنسبة لعمالة الأطفال

135
00:07:06,760 --> 00:07:07,139
صحيح

136
00:07:09,390 --> 00:07:13,999
الذي حدث للصورة المرسومة لبريطانيا بالقرن التاسع عشر،

137
00:07:14,000 --> 00:07:14,280
صحيح

138
00:07:15,420 --> 00:07:17,770
هو عدم وجود صورة لما حدث قبلها

139
00:07:17,771 --> 00:07:21,307
ولماذا قدم جميع هؤلاء الناس من الزراعة

140
00:07:21,308 --> 00:07:24,476
أو من المناطق الريفية إلى المدينة

141
00:07:24,477 --> 00:07:27,379
هل أتوا إلى المدينة لأنهم ظنوا أنها ستكون أسوأ؟

142
00:07:27,380 --> 00:07:29,748
أم بسبب ظنهم أنها ستكون أفضل؟

143
00:07:29,749 --> 00:07:31,717
وهل كانت أسوأ بالفعل أم أفضل؟

144
00:07:31,718 --> 00:07:32,970
قديما

145
00:07:32,970 --> 00:07:34,086
كما تعلم، لم يكن هناك

146
00:07:34,087 --> 00:07:35,329
أو أشياء قليلة للغاية

147
00:07:35,329 --> 00:07:37,957
التي كانت مئة بالمئة سوداء أو مئة بالمئة بيضاء

148
00:07:37,958 --> 00:07:38,257
صحيح

149
00:07:38,238 --> 00:07:39,280
كان هناك درجات متباينة من الرمادي

150
00:07:39,660 --> 00:07:43,195
وهدفنا هو الحصول على أقل درجة ممكنة من الرمادي

151
00:07:43,196 --> 00:07:47,384
لن أقول إن الأمور كانت وردية في بريطانيا وقتها، لأنها لم تكن كذلك.

152
00:07:47,634 --> 00:07:49,890
لكن أنظر إلى العالم اليوم

153
00:07:50,460 --> 00:07:52,406
أين هي الأحوال الأقل ورديةً

154
00:07:52,407 --> 00:07:54,660
ستجدها فالبلاد التي تدار من قبل الحكومة.

155
00:07:54,660 --> 00:07:56,752
ليست في البلاد التي تديرها المؤسسات الخاصة

156
00:07:56,753 --> 00:07:57,602
إذا أنت...

157
00:07:57,603 --> 00:07:59,445
ونفس الشيء كان صحيحاً في بريطانيا

158
00:07:59,446 --> 00:08:03,282
الأحوال بالمناطق النائية والمزارع

159
00:08:03,283 --> 00:08:05,718
كانت أسوأ بمراحل عن الأحوال بالمدن

160
00:08:05,719 --> 00:08:07,219
ولكنهم لم يكونوا ملحوظين

161
00:08:07,220 --> 00:08:09,030
كانوا مخفيين بحيث لم يراهم أحد

162
00:08:09,120 --> 00:08:12,674
ديكنز لم يتجول حول الريف حاملاً دفتر ملاحظاته

163
00:08:12,780 --> 00:08:13,255
صحيح

164
00:08:13,256 --> 00:08:14,700
حسنا، إذا الذي تقوله

165
00:08:14,700 --> 00:08:15,780
هو أن تلك الصورة الذهنية

166
00:08:16,230 --> 00:08:17,230
التي أدت إلى حد هذه اللحظة

167
00:08:17,231 --> 00:08:20,934
إلى الكثير من الجدل البيئي هي ببساطة

168
00:08:20,935 --> 00:08:23,102
قد تكون صحيحة إلى حد ما

169
00:08:23,103 --> 00:08:25,560
ولكنك قد تنصح بدراسة التاريخ بعمق أكثر

170
00:08:25,620 --> 00:08:26,606
ولكن ليس تاريخياً فقط،

171
00:08:26,607 --> 00:08:29,020
أين توجد أكثر المناطق تلوثا في العالم في يومنا هذا

172
00:08:29,020 --> 00:08:29,720
اليوم؟

173
00:08:29,776 --> 00:08:30,677
نعم اليوم

174
00:08:31,117 --> 00:08:31,910
في روسيا

175
00:08:31,911 --> 00:08:33,279
صحيح روسيا

176
00:08:33,280 --> 00:08:34,780
لماذا؟

177
00:08:34,781 --> 00:08:37,349
لأن كل شيء في روسيا تحت تصرف الحكومة

178
00:08:37,350 --> 00:08:38,584
ولم يكن هنالك...

179
00:08:38,585 --> 00:08:40,687
وسأظل أؤكد

180
00:08:40,688 --> 00:08:45,972
لا أحد سيعتني بملكية شخص آخر كما سيعتني بلمكيته

181
00:08:47,091 --> 00:08:50,029
لكن من سيتولى الموارد التي نتشاركها جميعا

182
00:08:50,030 --> 00:08:52,164
مثل الهواء الذي نتنفسه

183
00:08:52,145 --> 00:08:54,875
درس في التنفس

184
00:08:54,900 --> 00:08:56,902
أرغب بالضغط عليك مرة أخرى للحديث عن البيئة

185
00:08:56,903 --> 00:08:57,970
الهواء

186
00:08:58,131 --> 00:08:59,838
هنا في كاليفورنيا

187
00:08:59,839 --> 00:09:02,442
تبين أن هناك ثلاثة ملايين شخص يرغبون في التنفس

188
00:09:02,443 --> 00:09:05,880
وبالتحديد لدينا في حوض لوس أنجلوس

189
00:09:06,520 --> 00:09:08,348
بداية الضباب الدخاني في السبعينيات

190
00:09:08,349 --> 00:09:11,118
وأن الحركة البيئة بدأت بـ...

191
00:09:11,119 --> 00:09:13,440
أكثر من ذلك، يعود إلى مئتي سنة مضوا

192
00:09:13,980 --> 00:09:17,322
هناك قصص عن الهنود وهم يصفون هذه المنطقة بأنها ملوثة بالضباب الدخاني

193
00:09:17,760 --> 00:09:20,659
إذاً جزء من سبب حدوث ذلك هو الطبيعة

194
00:09:20,660 --> 00:09:21,795
لكن لا يوجد شك

195
00:09:21,796 --> 00:09:25,364
بأن هناك حجة

196
00:09:25,365 --> 00:09:32,384
للحكومة بأن تطالب من يفرضون تكاليف على جهات أخرى أن يتحملوها

197
00:09:32,841 --> 00:09:34,807
وبما يخص بالضباب الدخاني

198
00:09:34,808 --> 00:09:37,276
فإن الطريقة الفعالة لعمل ذلك هي استخدام السوق

199
00:09:37,599 --> 00:09:40,523
وكيف يمكن خلق حقوق ملكية للهواء؟

200
00:09:40,524 --> 00:09:41,748
حسنا، أنت تقوم بذلك بالفعل حالياً

201
00:09:41,749 --> 00:09:46,420
عن طريق بيع الكمية المسموح بها لانبعاث الملوثات في الجو

202
00:09:46,421 --> 00:09:50,790
فإن لديك الان سوق لحقوق التصريف

203
00:09:50,820 --> 00:09:53,450
من أجل الصناعات الكبرى

204
00:09:53,451 --> 00:09:56,662
نعم من أجل الصناعات الكبيرة والتي ينتج عنها المعظم

205
00:09:56,663 --> 00:09:59,733
وتفعل ذلك من خلال تغريمهم

206
00:09:59,734 --> 00:10:04,260
عبر فرض استخدام المركبات

207
00:10:06,275 --> 00:10:08,207
للمحول التحفيزي (تصفية الدخان)

208
00:10:08,207 --> 00:10:08,675
حسناً

209
00:10:08,675 --> 00:10:12,879
وفي الواقع جعل الأفراد مسؤلين

210
00:10:12,880 --> 00:10:14,714
عن التكلفة التي يفرضونها على الآخرين

211
00:10:14,715 --> 00:10:16,348
تذكر ما قلته بأن

212
00:10:16,349 --> 00:10:18,835
المبدأ الرئيسي لوجهة النظر الليبرتارية

213
00:10:19,105 --> 00:10:20,619
هو كونك حر لفعل ما تريد

214
00:10:20,620 --> 00:10:24,356
بشرط ألا تمنع الآخرين من فعل نفس الشيء

215
00:10:24,357 --> 00:10:26,409
والحالة الوحيدة للحكومة

216
00:10:26,654 --> 00:10:32,425
هي عندما تعجز أعراف السوق

217
00:10:33,084 --> 00:10:37,670
عن إلزام الأفراد بالدفع ...

218
00:10:37,671 --> 00:10:40,239
تعويض الآخرين عن أي أذى يعرضونهم له

219
00:10:40,240 --> 00:10:45,577
إذا كلانا اتفقنا على بيع أو شراء شيئاً ما

220
00:10:45,578 --> 00:10:47,365
هذه هي شؤوننا الخاصة

221
00:10:47,485 --> 00:10:47,781
صحيح

222
00:10:47,782 --> 00:10:49,483
قد تخسر أو قد أخسر

223
00:10:49,484 --> 00:10:51,617
أو من المرجح أننا سوف نفوز

224
00:10:51,618 --> 00:10:55,335
لن ندخل إلى هذا ما لم نعتقد أنه أفضل لنا

225
00:10:55,735 --> 00:10:57,235
ولكن في بعض الأحيان مثل

226
00:10:59,095 --> 00:11:03,562
محطة الطاقة التي تبعث دخان يلوث قميصي

227
00:11:03,553 --> 00:11:07,455
حيث أن الشركة تكلفني أموال

228
00:11:07,466 --> 00:11:10,702
ولا تعوضني عنها

229
00:11:10,703 --> 00:11:12,537
هذه هي الحالات فقط

230
00:11:12,538 --> 00:11:14,935
ولكن عليك توضيح ذلك

231
00:11:15,595 --> 00:11:18,834
عن طريق الإقرار بأن عندما تتدخل الحكومة

232
00:11:18,835 --> 00:11:20,680
فهي أيضا تبعث دخان

233
00:11:20,681 --> 00:11:23,083
وأيضا تقوم بفرض تكاليف على طرف ثالث

234
00:11:23,084 --> 00:11:27,535
والسبب هو كونه اتفاق غير مثالي دائما

235
00:11:28,225 --> 00:11:30,722
وأيضا يجب عليها جمع الضرائب دائما

236
00:11:30,723 --> 00:11:32,635
في خلال عملية تحصيل الضرائب

237
00:11:32,635 --> 00:11:34,561
هي دائما كما أقول

238
00:11:34,562 --> 00:11:37,615
هناك مخلفات دخانية خلف جميع البرامج الحكومية

239
00:11:38,965 --> 00:11:41,299
مخلفات دخانية خلف جميع البرامج الحكومية

240
00:11:41,300 --> 00:11:42,834
وهنا تقصد وجود تشوه

241
00:11:42,835 --> 00:11:43,235
صحيح

242
00:11:43,236 --> 00:11:44,946
في السوق

243
00:11:45,125 --> 00:11:48,141
فرض تكلفة على طرف ثالث

244
00:11:48,142 --> 00:11:50,455
دون تعويضه عنها

245
00:11:50,755 --> 00:11:52,795
إذا السمة الرئيسية

246
00:11:53,275 --> 00:11:56,748
أن تعثر على ظرف بحيث يكون تدخل الحكومة مباح

247
00:11:56,749 --> 00:11:59,534
وذلك عندما تصبح حقوق الملكية مبهمة أو مشتتة

248
00:11:59,534 --> 00:12:00,499
هل هذا صحيح؟

249
00:12:00,500 --> 00:12:03,755
وحين يصبح من المستحيل تحديدها بدقة

250
00:12:03,756 --> 00:12:06,892
هذه هي المشكلة في حالة محطة الطاقة

251
00:12:06,893 --> 00:12:07,465
حسنا

252
00:12:07,465 --> 00:12:09,355
هو كونه من المستحيل

253
00:12:09,685 --> 00:12:10,564
أن تقول

254
00:12:10,565 --> 00:12:12,865
يجب عليك الاتفاق مع جميع الأشخاص

255
00:12:13,135 --> 00:12:14,467
الذين ستلوث قميصهم

256
00:12:14,468 --> 00:12:15,033
صحيح

257
00:12:15,034 --> 00:12:18,437
وتدفع لهم مقابل امتياز تلويث قمصانهم قبل أن تتمكن من فعل ذلك

258
00:12:18,438 --> 00:12:19,015
إذا

259
00:12:19,675 --> 00:12:23,976
بخصوص البيئة فإن المدافعون عنها على صواب

260
00:12:23,977 --> 00:12:26,215
وهذه المساحة حيث توجد حجة قوية

261
00:12:26,245 --> 00:12:28,847
لكن في أغلب الحالات عملياً، عندما تنظر إليها

262
00:12:28,848 --> 00:12:31,315
وهناك بعض الناس في بيركنز كما تعلم

263
00:12:31,315 --> 00:12:32,317
صحيح

264
00:12:32,318 --> 00:12:36,027
الذين لديهم تيري انديرسون وأنا واثق بأنه ظهر ببرنامجك

265
00:12:36,028 --> 00:12:36,235
نعم

266
00:12:37,765 --> 00:12:40,735
الذي قام بتسليط الضوء حول وجود العديد من الحالات

267
00:12:40,735 --> 00:12:44,696
بحيث أن الأعراف السوقية تصبح أكثر فاعلية

268
00:12:44,697 --> 00:12:46,915
من اتفاقات القيادة والتحكم

269
00:12:46,945 --> 00:12:48,967
حسنا، إذا الان اصبح لدينا قليل من...

270
00:12:48,968 --> 00:12:51,145
لكن يجب أن يكون هناك بعض المجالات مثل الطعام والدواء

271
00:12:51,205 --> 00:12:53,155
حيث إن الصحة العامة تؤمّن فقط

272
00:12:53,155 --> 00:12:55,075
من خلال تدخل الحكومة، أليس كذلك؟

273
00:12:57,985 --> 00:13:00,478
إدارة الغذاء والدواء التي تنظم كل شيء

274
00:13:00,479 --> 00:13:03,115
بداية من الأدوية التي تضعها

275
00:13:03,115 --> 00:13:03,983
شركات الأدوية بالسوق

276
00:13:03,984 --> 00:13:08,955
حتى مكونات الأشياء التي نشتريها من رفوف البقالة

277
00:13:08,956 --> 00:13:11,057
دعني أعطيك مثالا

278
00:13:11,058 --> 00:13:12,044
الثاليدوميد

279
00:13:12,045 --> 00:13:14,905
المثال المفضل للجميع

280
00:13:14,905 --> 00:13:17,495
حسناً، قد أكون هجومياً بعض الشيء بهذا المثال

281
00:13:17,496 --> 00:13:18,864
ولكنني سأستخدمه على أية حال

282
00:13:18,865 --> 00:13:22,015
سُوّق في الخمسينيات والستينيات في أوروبا

283
00:13:22,585 --> 00:13:24,595
كدواء يساعد المرأة

284
00:13:24,595 --> 00:13:27,406
علي تخطي الغثيان المصاحب للحمل في بعض الأحيان

285
00:13:27,407 --> 00:13:29,245
إدارة الغذاء والدواء

286
00:13:29,245 --> 00:13:31,609
قالت أنه لم يجرّب كفاية بالولايات المتحدة

287
00:13:31,610 --> 00:13:34,013
ومُنع تسويقه داخل البلاد

288
00:13:34,014 --> 00:13:35,335
ونتيجة عن ذلك

289
00:13:36,655 --> 00:13:41,119
وُلد آلاف الأطفال بتشوهات فظيعة في أوروبا

290
00:13:41,120 --> 00:13:43,423
لأمهات استخدمن الثاليدوميد

291
00:13:43,424 --> 00:13:45,925
لكن هذا لم يحدث للأطفال الأمريكان

292
00:13:45,926 --> 00:13:49,627
لأن هيئة الدواء والغذاء تدخلت وحجبت الدواء عن الأسواق

293
00:13:49,628 --> 00:13:51,295
نشكر الله على وجود إدارة الدواء والغذاء

294
00:13:51,355 --> 00:13:52,030
أليس كذلك؟

295
00:13:52,031 --> 00:13:52,397
خطأ

296
00:13:52,398 --> 00:13:53,866
حسنا، لماذا؟

297
00:13:53,867 --> 00:13:55,968
في هذه الحالة قاموا بإنقاذ الأرواح

298
00:13:55,969 --> 00:13:57,670
وهذا سيناريو جيد

299
00:13:57,671 --> 00:14:02,635
ولكن إلى حد مماثل هم بطيئون في اعتماد دواء

300
00:14:03,235 --> 00:14:06,144
يتبين أنه سيصبح جيد جدا ومفيد

301
00:14:06,155 --> 00:14:09,514
كيف ستتمكن من توقع عدد الأرواح المفقودة بسبب ذلك

302
00:14:09,515 --> 00:14:12,745
أنت موظف في إدارة الغذاء والدواء

303
00:14:12,655 --> 00:14:13,248
نعم

304
00:14:13,359 --> 00:14:16,379
ولديك تساؤل حول الموافقة أو رفض دواء جديد

305
00:14:17,155 --> 00:14:18,324
إذا وافقت عليه

306
00:14:18,325 --> 00:14:22,475
وتبين أنه دواء مضر مثل الثاليدوميد

307
00:14:22,476 --> 00:14:23,996
ستقع في ورطة

308
00:14:23,997 --> 00:14:25,751
واسمك سيصبح على كل الصفحات الأولى

309
00:14:25,798 --> 00:14:26,905
سيكلفني ذلك وظيفتي

310
00:14:26,905 --> 00:14:28,864
وسأستدعى للكونجرس حتى أدلي بشهادتي

311
00:14:28,865 --> 00:14:33,385
على الجانب الآخر، إذا رفضت الدواء وتبين أنه جيد

312
00:14:34,435 --> 00:14:37,365
حسنا، ثم تعتمده لاحقاً عقب أربع أو خمس سنوات

313
00:14:38,365 --> 00:14:42,144
لن يشتكي أحد من حقيقة عدم اعتمادك له سابقا

314
00:14:42,145 --> 00:14:45,318
ما عدا شركات الأدوية الجشعة

315
00:14:45,319 --> 00:14:47,986
والتي ترغب في جني الأرباح على حساب العامة

316
00:14:47,987 --> 00:14:50,965
كما المقولة الشائعة بين الناس

317
00:14:51,745 --> 00:14:52,925
وإذا النتيجة هي

318
00:14:52,926 --> 00:14:57,396
أن الضغط على إدارة الغذاء والدواء يجعلها تؤجل اعتماد الدواء

319
00:14:57,775 --> 00:14:59,999
وهناك دليل ضخم

320
00:15:00,000 --> 00:15:05,238
بأنهم تسببوا في وفيات أكثر بسبب تأخرهم في الاعتماد

321
00:15:05,239 --> 00:15:07,573
مما أنقذوا باعتمادهم المبكر

322
00:15:07,574 --> 00:15:09,641
إذا وجهة نظرك هي القضاء على إدارة الغذاء والدواء

323
00:15:09,642 --> 00:15:10,435
بالتأكيد

324
00:15:10,765 --> 00:15:13,745
وما الذي سيحل محله؟

325
00:15:13,746 --> 00:15:16,348
سيحل محله المصلحة الشخصية لشركات الأدوية

326
00:15:16,349 --> 00:15:18,550
بتجنب حدوث هذه الأشياء السيئة

327
00:15:18,551 --> 00:15:23,990
هل تظن أن مُصنّع الثاليدوميد ربح أم خسر من صناعته

328
00:15:24,151 --> 00:15:25,275
أرى

329
00:15:25,505 --> 00:15:26,790
يجب عليك

330
00:15:26,791 --> 00:15:30,363
يجب أن يتحمل الناس مسؤولية الضرر الذي يسببونه

331
00:15:30,364 --> 00:15:32,113
يجب أن يكون ممكناً

332
00:15:32,114 --> 00:15:34,065
إذا قانون الضرر يتكفل بجزء كبير من ذلك

333
00:15:34,066 --> 00:15:34,801
بالتأكيد

334
00:15:34,802 --> 00:15:35,234
حسنا

335
00:15:35,235 --> 00:15:40,105
إذا قام ليلي أو ميرك أو أحد آخر باختراع دواء يضرني

336
00:15:40,106 --> 00:15:44,104
سأتعقبهم، سأشترك في دعوى جماعية مع كل من

337
00:15:44,105 --> 00:15:45,611
تناول ذلك العقار

338
00:15:45,612 --> 00:15:47,575
وسوف نقاضيهم بمليارات من الدولارات

339
00:15:47,575 --> 00:15:49,214
ونستحوذ على نصيب حاملي أسهمهم

340
00:15:49,215 --> 00:15:49,945
بالتأكيد

341
00:15:50,095 --> 00:15:50,650
برؤية ذلك

342
00:15:50,651 --> 00:15:55,075
يصبون اهتمامهم بالكامل لكي يكونوا صارمين بشدة في اختبار هذا الدواء

343
00:15:55,075 --> 00:15:55,821
قبل طرحه في السوق

344
00:15:55,822 --> 00:15:57,322
دعني أعطيك مثالاً آخر

345
00:15:57,323 --> 00:15:58,057
حسناً

346
00:15:58,058 --> 00:16:02,034
القواعد المفروضة على خطوط الطيران من أجل الأمان

347
00:16:02,035 --> 00:16:05,405
من هو المستفيد الأكبر من منع حوادث الطائرات

348
00:16:06,205 --> 00:16:08,535
بعد الراكب، خطوط الطيران؟

349
00:16:08,536 --> 00:16:12,737
حسنا، لا يبدو كون اهتمام الركاب أكثر من خطوط الطيران أمراً واضحاً على الاطلاق

350
00:16:12,738 --> 00:16:13,105
نعم

351
00:16:13,255 --> 00:16:16,104
لأن الطيارين هم جزء من الركاب

352
00:16:16,105 --> 00:16:17,677
بالطبع

353
00:16:17,678 --> 00:16:21,535
لماذا ستقوم الحكومة بتحسين أمان الخطوط الجوية؟

354
00:16:21,565 --> 00:16:22,725
وكيف ستقوم بذلك؟

355
00:16:23,114 --> 00:16:26,714
كيف يحفزون أي شخص لكي يحسن أمان الخطوط الجوية

356
00:16:26,715 --> 00:16:27,895
بقدر ما أستطيع أن أقول

357
00:16:27,925 --> 00:16:31,205
هل ميلتون فريدمان يعارض جميع ضوابط الصحة والأمان

358
00:16:31,205 --> 00:16:33,091
دعوني أجرب هذا عليه

359
00:16:33,092 --> 00:16:37,815
أليس من حق العامة أن يعرفوا حول المحتوى الغذائي للطعام الذي يشترونه؟

360
00:16:41,005 --> 00:16:43,835
السمنة هي مشكلة كبيرة في هذا البلد

361
00:16:43,836 --> 00:16:47,175
ولكن حتى سنوات قريبة

362
00:16:47,185 --> 00:16:50,575
كان من الصعب على متبعي الحمية الغذائية أن يلتقطوا عبوة من البقالة

363
00:16:50,576 --> 00:16:52,043
ويتعرفوا على المكونات

364
00:16:52,044 --> 00:16:55,914
ما هو محتوى الكربوهيدرات، الدهون، والسعرات الحرارية وما إلى ذلك

365
00:16:55,915 --> 00:16:57,865
إذا الحكومة تفرض

366
00:16:58,545 --> 00:17:03,595
قوانين بسيطة إلى حد ما من أجل طباعة القيم الغذائية

367
00:17:03,895 --> 00:17:05,455
على العبوات في البقالة حاليا

368
00:17:05,455 --> 00:17:08,293
يمكنك رؤية أن هذا فيه دهون أكثر و ذاك أقل سأشتريه

369
00:17:08,294 --> 00:17:10,315
أليس هذا تدخل حكومي

370
00:17:10,305 --> 00:17:12,230
بسيط ومقبول تماماً

371
00:17:12,240 --> 00:17:13,031
ولكن دعنا نستمر

372
00:17:13,032 --> 00:17:13,632
حسنا

373
00:17:13,633 --> 00:17:17,742
الحكومة كذلك تمنع ذكر معلومات مفيدة أيضا

374
00:17:17,743 --> 00:17:19,704
دعني أعطيك أبسط مثال

375
00:17:19,705 --> 00:17:20,205
حسناً

376
00:17:20,206 --> 00:17:21,673
الأسبرين

377
00:17:21,674 --> 00:17:23,185
جميعنا نعلم

378
00:17:23,935 --> 00:17:27,455
أن تناول الأسبرين يوميا أمر منصوح به

379
00:17:27,456 --> 00:17:29,879
لتقليل خطر الإصابة بالذبحة الصدرية

380
00:17:29,860 --> 00:17:30,215
نعم

381
00:17:30,216 --> 00:17:33,801
ولكن ذكر ذلك ممنوع على عبوة الأسبرين

382
00:17:33,801 --> 00:17:35,911
بسبب من؟

383
00:17:35,912 --> 00:17:37,489
إدارة الغذاء والدواء تمنع ذلك

384
00:17:37,490 --> 00:17:43,662
إنهم يتحكمون بالمعلومات التي تسجل على ملصق البيانات

385
00:17:43,663 --> 00:17:43,975
نعم

386
00:17:43,976 --> 00:17:49,315
الآن هناك بعض مصنعي الأدوية الليبريتارين

387
00:17:49,915 --> 00:17:53,271
الذين اقترحوا... حاولوا تمرير

388
00:17:53,272 --> 00:17:56,708
فكرة وضع مقولتهم:

389
00:17:56,729 --> 00:17:59,665
“هذا ما تقوله إدارة الغذاء والدواء وهذا ما نقوله نحن”

390
00:17:59,699 --> 00:18:00,864
“اختر”

391
00:18:01,705 --> 00:18:03,382
إنهم ممنوعون من عمل ذلك

392
00:18:03,383 --> 00:18:04,382
إنهم ممنوعون من عمل ذلك حتى

393
00:18:04,383 --> 00:18:09,305
إذا أراد العملاء حقاً معرفة المكونات

394
00:18:09,655 --> 00:18:14,292
سيكون من مصلحة المنتجين وضعها على عبواتهم

395
00:18:14,293 --> 00:18:19,505
العبوات التي تحمل المكونات ستصبح أكثر جاذبية للمستهلكين أصحاب هذه الرغبة

396
00:18:20,245 --> 00:18:23,635
لكن الآن هناك لغز بالنسبة لي وهو لماذا يعتقد الناس

397
00:18:23,636 --> 00:18:27,739
أن بعض الخبراء بمكتب في واشنطن

398
00:18:27,740 --> 00:18:31,143
الذين لا يعرفوك أو يعرفوني أو يعرفوا أبناءنا

399
00:18:31,144 --> 00:18:33,578
يعلمون أكثر منك ومني

400
00:18:33,579 --> 00:18:35,784
ما الذي نريد أن نجده على عبواتنا

401
00:18:35,785 --> 00:18:37,045
وما الذي نريد من أبنائنا أن يعلموه

402
00:18:37,465 --> 00:18:39,235
مرة أخري بدون تمييز تخلص من

403
00:18:39,265 --> 00:18:41,152
إدارة الغذاء والدواء وتخلص من اللوائح الحكومية

404
00:18:41,153 --> 00:18:41,635
بالطبع

405
00:18:41,905 --> 00:18:43,285
إدارة الغذاء والدواء بدايةً

406
00:18:44,935 --> 00:18:49,761
كانت مكلفة بالتأكد من أمان

407
00:18:49,762 --> 00:18:53,698
وليس فاعلية الأدوية التي يعتمدونها

408
00:18:53,699 --> 00:18:55,465
وبما يسمى

409
00:18:55,475 --> 00:18:56,468
تعديلات كيفوفر

410
00:18:56,469 --> 00:18:58,371
والذي نتج عن الثاليدوميد

411
00:18:58,372 --> 00:18:59,604
الذي ذكرته

412
00:18:59,605 --> 00:19:03,775
إدارة الغذاء والدواء وسعت سلطتها

413
00:19:03,775 --> 00:19:06,845
لتشمل التأكد من أمان

414
00:19:06,846 --> 00:19:09,915
وفعالية الأدوية

415
00:19:09,916 --> 00:19:13,818
وهذا زاد من تكلفة اعتماد الأدوية بشكل مهول

416
00:19:13,819 --> 00:19:14,553
أرى...

417
00:19:14,554 --> 00:19:17,123
وإذا أردت الحصول على حل وسط

418
00:19:17,124 --> 00:19:19,092
يمكنك العودة إلى الأساس السابق

419
00:19:19,093 --> 00:19:22,328
عندما كانت إدارة الغذاء والدواء توثق الأمان

420
00:19:22,329 --> 00:19:25,697
ولكن لم تحكم على الفاعلية

421
00:19:25,698 --> 00:19:27,594
إدارة الغذاء والدواء تؤكد ببساطة على أن

422
00:19:27,594 --> 00:19:29,901
شركات الأدوية تلتزم بالمقولة المأثورة

423
00:19:29,902 --> 00:19:30,936
"لا تسببوا ضرر"

424
00:19:30,937 --> 00:19:31,369
نعم

425
00:19:31,370 --> 00:19:33,738
هذا الدواء لن يغير حياتك، ولكنه لن يؤذيك

426
00:19:33,739 --> 00:19:35,173
لذا ربما يُسوّق

427
00:19:35,174 --> 00:19:35,374
نعم

428
00:19:35,375 --> 00:19:35,908
حسنا

429
00:19:35,909 --> 00:19:37,243
الآن دعني أنتقل إلى قضية أخرى

430
00:19:37,244 --> 00:19:41,546
وأظن أن هذه صعبة نوعا ما على الليبرتاري

431
00:19:41,547 --> 00:19:42,381
حسنا

432
00:19:42,382 --> 00:19:43,848
لذا مسموح لك بأخذ نفس عميق

433
00:19:43,849 --> 00:19:45,444
قبل أن أصدمك بهذا إذا رغبت

434
00:19:45,444 --> 00:19:47,153
الحقوق المدنية

435
00:19:47,164 --> 00:19:49,188
ما الذي تقصده بالحقوق المدنية

436
00:19:49,189 --> 00:19:51,235
الذي أقصده هو

437
00:19:52,645 --> 00:19:57,684
لنأخذ قضية الجنوب تحت حكم جيم كرو في الخمسينيات

438
00:19:57,685 --> 00:20:00,565
لكن هذه كانت قضية حكومة ... الكثير من التدخل الحكومي

439
00:20:00,566 --> 00:20:01,705
كانت كذلك؟

440
00:20:01,705 --> 00:20:04,335
ظننت أن الضرائب كانت منخفضة نسبيا في تلك الأيام في الجنوب

441
00:20:04,336 --> 00:20:05,554
وكانت الأنظمة أقل نسبيا

442
00:20:05,555 --> 00:20:09,441
لا، لا، الحكومة دعمت العزل

443
00:20:09,442 --> 00:20:14,839
الحكومة هي من ألزمت بأماكن منفصلة للبيض والسود

444
00:20:14,840 --> 00:20:18,280
الحكومة هي من طبقّت قانون

445
00:20:18,281 --> 00:20:20,852
إلزام السود بالجلوس في المقاعد الخلفية للحافلات

446
00:20:20,853 --> 00:20:22,153
كل هذه كانت قوانين حكومية

447
00:20:22,154 --> 00:20:24,189
بغياب هذه القوانين الحكومية

448
00:20:24,190 --> 00:20:25,053
لم يكن لهذا أن يحدث؟

449
00:20:25,054 --> 00:20:25,424
بكلمات أخرى..

450
00:20:25,425 --> 00:20:28,260
بغياب القوانين الحكومية كنت سترى تطورا تدريجيا

451
00:20:28,261 --> 00:20:30,510
ستراها في أماكن دون أخرى

452
00:20:30,970 --> 00:20:32,900
وانظر ما حدث في الشمال

453
00:20:32,901 --> 00:20:34,399
حيث غابت هذه القوانين الحكومية

454
00:20:34,400 --> 00:20:35,157
حسنا

455
00:20:35,158 --> 00:20:38,859
لربما كان هناك.. بلا شك.. لا تفهمي خطأً

456
00:20:38,860 --> 00:20:40,839
هناك تحيز، لا جدل في ذلك

457
00:20:40,840 --> 00:20:41,307
حسناً

458
00:20:41,308 --> 00:20:44,542
وبلا شك كان له تاثير سيئ على أناسٍ كثيرين

459
00:20:44,543 --> 00:20:48,356
لكن لو غابت القوانين في الجنوب

460
00:20:48,357 --> 00:20:51,116
لأنتهت المشكلة بزمن أقل ووقت أبكر

461
00:20:51,117 --> 00:20:56,902
إن كنت تريد حجة تدعم الليبرتارية فهي هذه

462
00:20:56,902 --> 00:20:57,656
حسنا

463
00:20:57,819 --> 00:21:01,690
أخبرنا ميلتون فريدمان لم يتعّين تقييد دور الحكومة في حياتنا

464
00:21:01,691 --> 00:21:03,261
لكن إلى أي حد؟

465
00:21:03,262 --> 00:21:07,270
دعونا نسأله عن هيكل الحكومة الفدرالية نفسها

466
00:21:07,487 --> 00:21:09,560
تعديلات وزارية

467
00:21:09,800 --> 00:21:13,538
لدي قائمة بالأقسام الوزارية الأربعة عشر

468
00:21:13,539 --> 00:21:17,430
أربعة عشر رقم كبير بالنسبة لبرنامج تلفزيوني، أريد أن أمر على القائمة بسرعة

469
00:21:17,431 --> 00:21:19,619
وأريدك أن تشير إلي بالموافقة أو الرفض

470
00:21:19,620 --> 00:21:21,230
أنبقيهم أم نتخلى عنهم

471
00:21:21,230 --> 00:21:22,614
وزارة الزراعة

472
00:21:22,605 --> 00:21:23,615
تخلّى

473
00:21:23,616 --> 00:21:24,190
ذهبت

474
00:21:24,530 --> 00:21:25,517
وزارة التجارة

475
00:21:25,518 --> 00:21:27,018
تخلّى

476
00:21:27,019 --> 00:21:27,587
ذهبت

477
00:21:27,588 --> 00:21:28,753
وزارة الدفاع

478
00:21:28,754 --> 00:21:29,688
أبق

479
00:21:29,689 --> 00:21:30,556
أبقها

480
00:21:30,557 --> 00:21:31,989
وزارة التعليم

481
00:21:31,990 --> 00:21:32,857
تخلّى

482
00:21:32,858 --> 00:21:33,157
ذهبت

483
00:21:33,158 --> 00:21:34,492
الطاقة

484
00:21:34,493 --> 00:21:35,159
تخلى

485
00:21:35,729 --> 00:21:39,098
لكن الطاقة مرتبطة بالجيش

486
00:21:39,099 --> 00:21:40,850
إذا ندفع بها تحت الدفاع

487
00:21:40,850 --> 00:21:42,300
القسم الذي يتولى النووي

488
00:21:42,301 --> 00:21:42,950
نعم

489
00:21:42,980 --> 00:21:43,774


490
00:21:43,775 --> 00:21:45,187
البلاتينيوم وماشابه يدخل تحت الدفاع

491
00:21:45,187 --> 00:21:46,372
لكننا نتخلى عن الباقي

492
00:21:46,373 --> 00:21:47,780
الصحة والخدمات الإنسانية

493
00:21:48,300 --> 00:21:52,130
هناك بعض المساحة للنشاطات المتعلقة بالصحة العامة

494
00:21:52,670 --> 00:21:55,010
لتجنب الأوبئة

495
00:21:56,240 --> 00:21:58,083
مثل..

496
00:21:58,084 --> 00:22:00,318
إذا ستبقي مؤسسة الصحة الوطنية NHI

497
00:22:00,319 --> 00:22:00,942
لا، لا، لا ليست ...

498
00:22:00,953 --> 00:22:01,850
مركز احتواء الأمراض

499
00:22:01,850 --> 00:22:04,490
لا، لا مؤسسة الصحة الوطنية... هذه هي وكالات بحثية

500
00:22:04,940 --> 00:22:10,150
لا، لا، هذا سؤال عما إذا كانت الحكومة ستشارك في تمويل الأبحاث

501
00:22:10,329 --> 00:22:11,564
والجواب هو لا؟

502
00:22:11,565 --> 00:22:14,566
حسنا، هذه معقدة، مسألة معقدة جدا

503
00:22:14,567 --> 00:22:16,670
وليس هناك جواب سهل بالنسبة لهذا

504
00:22:16,790 --> 00:22:18,459
إذا لنقضي على نصف وزارة الصحة؟

505
00:22:18,460 --> 00:22:19,581
نعم شيء مثل هذا

506
00:22:19,572 --> 00:22:20,420
إذا نصف فقط

507
00:22:20,420 --> 00:22:22,139
لنذهب إلى وزارة الأسكان والتطوير الحضاري

508
00:22:22,140 --> 00:22:22,740
أخرجها

509
00:22:22,751 --> 00:22:24,676
لم تتمهل أبدا في هذه

510
00:22:24,677 --> 00:22:25,511
وزارة الداخلية

511
00:22:25,512 --> 00:22:29,080
حسنا، لكن وزارة الإسكان والتطوير الحضري تسببت بضرر عظيم

512
00:22:29,081 --> 00:22:30,549
يا إلهي

513
00:22:30,550 --> 00:22:35,221
إذا فكرت بالطريقة التي دمروا فيها أجزاء من المدن

514
00:22:35,212 --> 00:22:39,014
تحت حجة التخلص من العشوائيات

515
00:22:39,035 --> 00:22:40,335
جاك....

516
00:22:40,326 --> 00:22:45,590
أنت تذكر وكتب مارك أندرسون كتاباً عن الجرافة الفدرالية

517
00:22:46,430 --> 00:22:49,168
يتحدث عن فعالية التطوير الحضري

518
00:22:49,169 --> 00:22:52,460
قد أزيلت مساكن أكثر

519
00:22:53,180 --> 00:22:56,719
مما بُني تحت اسم الإسكان العام

520
00:22:56,720 --> 00:23:00,500
اقترح جاك كامب بيع

521
00:23:01,130 --> 00:23:04,816
المساكن العامة لساكنيها

522
00:23:04,817 --> 00:23:08,519
بوحداتها ومساكنها وشققها مقابل دولار واحد

523
00:23:08,520 --> 00:23:11,349
والاكتفاء بنقل الملكية لمن يعيش فيها

524
00:23:11,350 --> 00:23:15,960
من المجدي أن نتخلص من وزارة الإسكان والتطوير الحضري

525
00:23:15,961 --> 00:23:16,850
حسنا، تمّ

526
00:23:17,060 --> 00:23:17,528
تخلينا عنها

527
00:23:17,529 --> 00:23:19,227
وزارة الداخلية

528
00:23:19,227 --> 00:23:21,666
خدمة الحدائق العامّة التي تحبها

529
00:23:21,667 --> 00:23:23,240
حسنا، أخذاً بـ

530
00:23:23,340 --> 00:23:27,709
المسألة هي أن عليك أولاً بيع كل الأراضي المملوكة للحكومة

531
00:23:27,710 --> 00:23:29,741
لكن هذا ما عليك فعله

532
00:23:29,742 --> 00:23:30,830
يمكن فعلها بسرعة

533
00:23:30,831 --> 00:23:32,644
عليك فعل ذلك

534
00:23:32,645 --> 00:23:34,390
لا مبرر للحكومة بأن تمتلك

535
00:23:34,520 --> 00:23:37,749
الحكومة تمتلك الآن ما يقارب ثلث كل الأراضي في البلاد

536
00:23:38,500 --> 00:23:39,785
وهذا مبالغ فيه

537
00:23:39,786 --> 00:23:41,319
يجب أن تنخفض إلى صفر

538
00:23:41,320 --> 00:23:43,790
يجب خفضها، ليس إلى صفر

539
00:23:44,180 --> 00:23:47,160
يتوجب عليهم امتلاك الأراضي التي عليها المباني الحكومية

540
00:23:48,380 --> 00:23:49,260
حسنا، هذا رائع

541
00:23:49,261 --> 00:23:50,261
وزارة العدل

542
00:23:50,262 --> 00:23:51,230
نعم

543
00:23:51,260 --> 00:23:51,896
أبق هذه

544
00:23:51,897 --> 00:23:52,447
أبقها

545
00:23:52,448 --> 00:23:53,365
العمل

546
00:23:53,366 --> 00:23:53,632
لا

547
00:23:53,633 --> 00:23:54,140
ذهبت

548
00:23:54,410 --> 00:23:55,033
الخارجية

549
00:23:55,034 --> 00:23:56,001
أبق

550
00:23:56,002 --> 00:23:56,400
أبقها

551
00:23:56,401 --> 00:23:57,468
النقل

552
00:23:57,469 --> 00:23:58,336
لتذهب

553
00:23:58,337 --> 00:23:58,850
ذهبت

554
00:24:00,080 --> 00:24:00,805
الخزانة

555
00:24:00,806 --> 00:24:03,016
عليك ابقاءها لجمع الضرائب

556
00:24:03,017 --> 00:24:04,575
حسنا، اجمع الضرائب عبر الخزانة

557
00:24:04,576 --> 00:24:05,476
شؤون قدامى المحاربين

558
00:24:05,477 --> 00:24:08,680
يمكنك اعتبار شؤون قدامى المحاربين كطريقة...

559
00:24:08,840 --> 00:24:14,685
لدفع رواتب لقاء خدمات من كانوا في القوات المسلحة

560
00:24:14,780 --> 00:24:16,249
لكن يمكنك التخلص منها

561
00:24:16,250 --> 00:24:19,109
بدفعها مقدماً

562
00:24:19,110 --> 00:24:20,539
دفع مجموع مبالغ مقدماً

563
00:24:20,540 --> 00:24:21,038
هذا صحيح

564
00:24:21,039 --> 00:24:21,994
والتخلص منها

565
00:24:21,995 --> 00:24:26,198
حسنا، ميلتون فريدمان، إذا جُعلت مستبداً ليوم واحدٍ

566
00:24:26,199 --> 00:24:27,374
في اليوم التالي...

567
00:24:27,374 --> 00:24:29,700
لا، لا، لا أريد أن أكون مستبدا

568
00:24:29,701 --> 00:24:30,080
لن تكون؟

569
00:24:30,110 --> 00:24:31,335
لا أؤمن بالمستبدين

570
00:24:31,336 --> 00:24:31,870
حسناً

571
00:24:31,871 --> 00:24:33,920
أنا أؤمن بالسعي وراء التغيير

572
00:24:34,520 --> 00:24:37,576
لكن بالاتفاق

573
00:24:37,577 --> 00:24:40,369
للمواطنين، أنا لا أؤمن بـ...

574
00:24:40,370 --> 00:24:41,846
دعني أضعها بهذه الصيغة

575
00:24:41,847 --> 00:24:43,030
اقتراحك

576
00:24:43,021 --> 00:24:44,840
إن لم نستطع حث العامّة

577
00:24:44,851 --> 00:24:47,486
على تفضيل القيام بهذه الأشياء

578
00:24:47,487 --> 00:24:51,050
فإنه ليس لدينا الحق بفرضها عليهم حتى ولو امتلكنا القوة لذلك

579
00:24:52,070 --> 00:24:55,460
من أربعة عشر قسماً إلى أربعة ونصف

580
00:24:55,580 --> 00:24:58,490
لمهامها الأساسية، ما هي مهامها الأساسية

581
00:24:59,390 --> 00:25:01,332
الحفاظ على السلام

582
00:25:01,333 --> 00:25:02,180
الدفاع عن البلاد

583
00:25:02,210 --> 00:25:02,700
حسناً

584
00:25:02,701 --> 00:25:07,271
إيجاد آلية للناس للتقاضي في خلافاتهم

585
00:25:07,272 --> 00:25:09,443
وهذه هي وزارة العدل

586
00:25:09,443 --> 00:25:14,579
حماية الناس من تعدي الآخرين

587
00:25:14,580 --> 00:25:16,050
الشرطة قامت بهذه الوظيفة

588
00:25:16,050 --> 00:25:16,847
حسنا

589
00:25:16,848 --> 00:25:19,580
والآن هذا يشمل

590
00:25:19,580 --> 00:25:23,988
كلا الحكومتين، المركزية والحكومات المحلية

591
00:25:23,989 --> 00:25:27,171
الشرطة تعمل أساساً محليا ومركزيا

592
00:25:27,172 --> 00:25:30,362
وهذه هي الوظائف الأساسية للحكومة في رأيي

593
00:25:30,363 --> 00:25:32,863
ميلتون فريدمان، شكراً جزيلاً لك

594
00:25:34,670 --> 00:25:37,069
يؤمن د. فريدمان بحكومة محدودة

595
00:25:37,070 --> 00:25:38,604
حكومة محدودة جداً

596
00:25:38,605 --> 00:25:40,572
إن كان فهمي لمبادئه صحيحاً

597
00:25:40,573 --> 00:25:43,042
فإنه سيقول بأن خيار استخدام خوذة الدراجة

598
00:25:43,043 --> 00:25:45,810
يجب أن لا يكون بيني وبين حكومة الولاية في سكرمنتو

599
00:25:45,811 --> 00:25:49,547
بل بيني وبين إن كان لأحد علاقة فهو شركة التأمين

600
00:25:49,548 --> 00:25:51,620
من الأفضل ابقاء الرسوم منخفضة

601
00:25:53,330 --> 00:25:55,430
أنا بيتر روبنسون، شكرا لوجودك معنا
